$(document).ready(()=>{
    

    
    

    $("#addition").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1+value2);
    })
    $("#sub").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1-value2);
    })
    $("#mult").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1*value2);
    })
    $("#div").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1/value2);
    })
    $("#mod").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1%value2);
    })
    $("#clear").click(()=>{
        $("#first-val").val(null)
        $("#second-val").val(null) 
        
        $(".output-box").html("");
    })

})